## Deployment steps doc : https://docs.google.com/document/d/1TeCcZuz9YNNaO_QtrTRT6mhdXQwtHhZ1HWAenEXpPkg/edit?usp=sharing
* [DiscoveryToken](#discoverytoken)
  * [name](#function-name)
  * [approve](#function-approve)
  * [tokensForSale](#function-tokensforsale)
  * [icoStartBlock](#function-icostartblock)
  * [totalSupply](#function-totalsupply)
  * [bountyTokens](#function-bountytokens)
  * [burnTokensForSale](#function-burntokensforsale)
  * [transferFrom](#function-transferfrom)
  * [decimals](#function-decimals)
  * [_totalSupply](#function-_totalsupply)
  * [unpause](#function-unpause)
  * [sendBounty](#function-sendbounty)
  * [burn](#function-burn)
  * [finalize](#function-finalize)
  * [partnersTokens](#function-partnerstokens)
  * [paused](#function-paused)
  * [vestingContract](#function-vestingcontract)
  * [treasuryTokens](#function-treasurytokens)
  * [advisorsTokens](#function-advisorstokens)
  * [balanceOf](#function-balanceof)
  * [acceptOwnership](#function-acceptownership)
  * [pause](#function-pause)
  * [frozenAccounts](#function-frozenaccounts)
  * [marketingTokens](#function-marketingtokens)
  * [owner](#function-owner)
  * [changeApproval](#function-changeapproval)
  * [releasePartnersTokens](#function-releasepartnerstokens)
  * [symbol](#function-symbol)
  * [tokensLocked](#function-tokenslocked)
  * [saleTransfer](#function-saletransfer)
  * [fundraising](#function-fundraising)
  * [transfer](#function-transfer)
  * [vestingTransfer](#function-vestingtransfer)
  * [releaseTreasuryTokens](#function-releasetreasurytokens)
  * [teamTokens](#function-teamtokens)
  * [activateSaleContract](#function-activatesalecontract)
  * [newOwner](#function-newowner)
  * [saleContract](#function-salecontract)
  * [transferAnyERC20Token](#function-transferanyerc20token)
  * [allowance](#function-allowance)
  * [totalReleased](#function-totalreleased)
  * [freezeAccount](#function-freezeaccount)
  * [transferOwnership](#function-transferownership)
  * [tokensUnlockPeriod](#function-tokensunlockperiod)
  * [FrozenFund](#event-frozenfund)
  * [PriceLog](#event-pricelog)
  * [ReleasedPartnerTokens](#event-releasedpartnertokens)
  * [ReleasedTreasuryTokens](#event-releasedtreasurytokens)
  * [Pause](#event-pause)
  * [Unpause](#event-unpause)
  * [OwnershipTransferred](#event-ownershiptransferred)
  * [Transfer](#event-transfer)
  * [Approval](#event-approval)
  * [Burn](#event-burn)
  * [SaleContractActivation](#event-salecontractactivation)
* [ERC20](#erc20)
  * [approve](#function-approve)
  * [totalSupply](#function-totalsupply)
  * [transferFrom](#function-transferfrom)
  * [balanceOf](#function-balanceof)
  * [transfer](#function-transfer)
  * [allowance](#function-allowance)
  * [Transfer](#event-transfer)
  * [Approval](#event-approval)
  * [Burn](#event-burn)
  * [SaleContractActivation](#event-salecontractactivation)
* [Owned](#owned)
  * [acceptOwnership](#function-acceptownership)
  * [owner](#function-owner)
  * [newOwner](#function-newowner)
  * [transferOwnership](#function-transferownership)
  * [OwnershipTransferred](#event-ownershiptransferred)
* [Pausable](#pausable)
  * [unpause](#function-unpause)
  * [paused](#function-paused)
  * [acceptOwnership](#function-acceptownership)
  * [pause](#function-pause)
  * [owner](#function-owner)
  * [newOwner](#function-newowner)
  * [transferOwnership](#function-transferownership)
  * [Pause](#event-pause)
  * [Unpause](#event-unpause)
  * [OwnershipTransferred](#event-ownershiptransferred)
* [SafeMath](#safemath)
* [StandardToken](#standardtoken)
  * [approve](#function-approve)
  * [totalSupply](#function-totalsupply)
  * [transferFrom](#function-transferfrom)
  * [burn](#function-burn)
  * [balanceOf](#function-balanceof)
  * [changeApproval](#function-changeapproval)
  * [transfer](#function-transfer)
  * [allowance](#function-allowance)
  * [Transfer](#event-transfer)
  * [Approval](#event-approval)
  * [Burn](#event-burn)
  * [SaleContractActivation](#event-salecontractactivation)

# DiscoveryToken


## *function* name

DiscoveryToken.name() `view` `06fdde03`





## *function* approve

DiscoveryToken.approve(_spender, _value) `nonpayable` `095ea7b3`

> Approve the passed address to spend the specified amount of tokens on behalf of msg.sender.   * Beware that changing an allowance with this method brings the risk that someone may use both the old and the new allowance by unfortunate transaction ordering. One possible solution to mitigate this race condition is to first reduce the spender's allowance to 0 and set the desired value afterwards: https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729

Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* | _spender | The address which will spend the funds. |
| *uint256* | _value | The amount of tokens to be spent. |


## *function* tokensForSale

DiscoveryToken.tokensForSale() `view` `12aef8c3`





## *function* icoStartBlock

DiscoveryToken.icoStartBlock() `view` `12ea965d`





## *function* totalSupply

DiscoveryToken.totalSupply() `view` `18160ddd`





## *function* bountyTokens

DiscoveryToken.bountyTokens() `view` `1a9bf9cf`





## *function* burnTokensForSale

DiscoveryToken.burnTokensForSale() `nonpayable` `1b65144d`

> this function will burn the unsold tokens after crowdsale is over and this can be called from crowdsale contract only when crowdsale is over




## *function* transferFrom

DiscoveryToken.transferFrom(_from, _to, _value) `nonpayable` `23b872dd`


Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* | _from | undefined |
| *address* | _to | undefined |
| *uint256* | _value | undefined |


## *function* decimals

DiscoveryToken.decimals() `view` `313ce567`





## *function* _totalSupply

DiscoveryToken._totalSupply() `view` `3eaaf86b`





## *function* unpause

DiscoveryToken.unpause() `nonpayable` `3f4ba83a`





## *function* sendBounty

DiscoveryToken.sendBounty(_to, _value) `nonpayable` `404704b9`

> this function will send the bounty tokens to given address

Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* | _to | ,address of the bounty receiver. |
| *uint256* | _value | , number of tokens to be sent. |


## *function* burn

DiscoveryToken.burn(_value) `nonpayable` `42966c68`

> Burns a specific amount of tokens.

Inputs

| **type** | **name** | **description** |
|-|-|-|
| *uint256* | _value | The amount of token to be burned. |


## *function* finalize

DiscoveryToken.finalize() `nonpayable` `4bb278f3`

> this function will closes the sale ,after this anyone can transfer their tokens to others.




## *function* partnersTokens

DiscoveryToken.partnersTokens() `view` `4e47f3a7`





## *function* paused

DiscoveryToken.paused() `view` `5c975abb`





## *function* vestingContract

DiscoveryToken.vestingContract() `view` `5e6f6045`





## *function* treasuryTokens

DiscoveryToken.treasuryTokens() `view` `5eebef6b`





## *function* advisorsTokens

DiscoveryToken.advisorsTokens() `view` `6078268b`





## *function* balanceOf

DiscoveryToken.balanceOf(_owner) `view` `70a08231`

> Returns number of tokens owned by given address

Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* | _owner | Address of token owner |

Outputs

| **type** | **name** | **description** |
|-|-|-|
| *uint256* |  | undefined |

## *function* acceptOwnership

DiscoveryToken.acceptOwnership() `nonpayable` `79ba5097`





## *function* pause

DiscoveryToken.pause() `nonpayable` `8456cb59`





## *function* frozenAccounts

DiscoveryToken.frozenAccounts() `view` `860838a5`


Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* |  | undefined |


## *function* marketingTokens

DiscoveryToken.marketingTokens() `view` `8b27306d`





## *function* owner

DiscoveryToken.owner() `view` `8da5cb5b`





## *function* changeApproval

DiscoveryToken.changeApproval(_spender, _oldValue, _newValue) `nonpayable` `9281cd65`


Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* | _spender | undefined |
| *uint256* | _oldValue | undefined |
| *uint256* | _newValue | undefined |


## *function* releasePartnersTokens

DiscoveryToken.releasePartnersTokens(_partner, _value) `nonpayable` `929cd57d`

> this function can only be called by owner to transfer partner tokens to any partner address

Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* | _partner | address The address of the partner. |
| *uint256* | _value | uint256 The amount of tokens to be send |


## *function* symbol

DiscoveryToken.symbol() `view` `95d89b41`





## *function* tokensLocked

DiscoveryToken.tokensLocked() `view` `a1feba42`





## *function* saleTransfer

DiscoveryToken.saleTransfer(_to, _value) `nonpayable` `a51a8682`

> this function can only be called by crowdsale contract to transfer tokens to investor

Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* | _to | address The address of the investor. |
| *uint256* | _value | uint256 The amount of tokens to be send |


## *function* fundraising

DiscoveryToken.fundraising() `view` `a70fc680`





## *function* transfer

DiscoveryToken.transfer(_to, _value) `nonpayable` `a9059cbb`


Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* | _to | undefined |
| *uint256* | _value | undefined |


## *function* vestingTransfer

DiscoveryToken.vestingTransfer(_to, _value) `nonpayable` `b4842244`

> this function can only be called by  contract to transfer tokens to vesting beneficiary

Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* | _to | address The address of the beneficiary. |
| *uint256* | _value | uint256 The amount of tokens to be send |


## *function* releaseTreasuryTokens

DiscoveryToken.releaseTreasuryTokens(_to, _value) `nonpayable` `c268d981`

> this function can only be called by owner to transfer treasury tokens to any  address

Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* | _to | address The address of the token receiver. |
| *uint256* | _value | uint256 The amount of tokens to be send |


## *function* teamTokens

DiscoveryToken.teamTokens() `view` `c3e3c7bc`





## *function* activateSaleContract

DiscoveryToken.activateSaleContract(_saleContract, _vestingContract) `nonpayable` `c9da86f5`

> activates the sale and vesting contract (i.e. transfers saleable and vestable tokens to respective contracts)

Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* | _saleContract | ,address of crowdsale contract |
| *address* | _vestingContract | ,address of the vesting contract |


## *function* newOwner

DiscoveryToken.newOwner() `view` `d4ee1d90`





## *function* saleContract

DiscoveryToken.saleContract() `view` `daf6ca30`





## *function* transferAnyERC20Token

DiscoveryToken.transferAnyERC20Token(tokenAddress, tokens) `nonpayable` `dc39d06d`

> Function to transfer any ERC20 token  to owner address which gets accidentally transferred to this contract

Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* | tokenAddress | The address of the ERC20 contract |
| *uint256* | tokens | The amount of tokens to transfer. |

Outputs

| **type** | **name** | **description** |
|-|-|-|
| *bool* | success | undefined |

## *function* allowance

DiscoveryToken.allowance(_owner, _spender) `view` `dd62ed3e`

> Function to check the amount of tokens that an owner allowed to a spender.

Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* | _owner | address The address which owns the funds. |
| *address* | _spender | address The address which will spend the funds. |

Outputs

| **type** | **name** | **description** |
|-|-|-|
| *uint256* |  | undefined |

## *function* totalReleased

DiscoveryToken.totalReleased() `view` `e33b7de3`





## *function* freezeAccount

DiscoveryToken.freezeAccount(target, freeze) `nonpayable` `e724529c`

> this function will freeze the any account so that the frozen account will not able to participate in crowdsale.

Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* | target | ,address of the target account  |
| *bool* | freeze | ,boolean value to freeze or unfreeze the account ,true to freeze and false to unfreeze |


## *function* transferOwnership

DiscoveryToken.transferOwnership(newOwner) `nonpayable` `f2fde38b`

> Allows the current owner to transfer control of the contract to a newOwner.

Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* | newOwner | The address to transfer ownership to. |


## *function* tokensUnlockPeriod

DiscoveryToken.tokensUnlockPeriod() `view` `f3aee9f9`






## *event* FrozenFund

DiscoveryToken.FrozenFund(target, frozen) `5ea9caff`

Arguments

| **type** | **name** | **description** |
|-|-|-|
| *address* | target | not indexed |
| *bool* | frozen | not indexed |

## *event* PriceLog

DiscoveryToken.PriceLog(text) `8cc513ff`

Arguments

| **type** | **name** | **description** |
|-|-|-|
| *string* | text | not indexed |

## *event* ReleasedPartnerTokens

DiscoveryToken.ReleasedPartnerTokens(partner, tokens) `ac62924c`

Arguments

| **type** | **name** | **description** |
|-|-|-|
| *address* | partner | not indexed |
| *uint256* | tokens | not indexed |

## *event* ReleasedTreasuryTokens

DiscoveryToken.ReleasedTreasuryTokens(partner, tokens) `748802c9`

Arguments

| **type** | **name** | **description** |
|-|-|-|
| *address* | partner | not indexed |
| *uint256* | tokens | not indexed |

## *event* Pause

DiscoveryToken.Pause() `6985a022`



## *event* Unpause

DiscoveryToken.Unpause() `7805862f`



## *event* OwnershipTransferred

DiscoveryToken.OwnershipTransferred(from, _to) `8be0079c`

Arguments

| **type** | **name** | **description** |
|-|-|-|
| *address* | from | indexed |
| *address* | _to | indexed |

## *event* Transfer

DiscoveryToken.Transfer(from, to, value) `ddf252ad`

Arguments

| **type** | **name** | **description** |
|-|-|-|
| *address* | from | indexed |
| *address* | to | indexed |
| *uint256* | value | not indexed |

## *event* Approval

DiscoveryToken.Approval(owner, spender, value) `8c5be1e5`

Arguments

| **type** | **name** | **description** |
|-|-|-|
| *address* | owner | indexed |
| *address* | spender | indexed |
| *uint256* | value | not indexed |

## *event* Burn

DiscoveryToken.Burn(from, value) `cc16f5db`

Arguments

| **type** | **name** | **description** |
|-|-|-|
| *address* | from | indexed |
| *uint256* | value | not indexed |

## *event* SaleContractActivation

DiscoveryToken.SaleContractActivation(saleContract, tokensForSale) `9ca62f58`

Arguments

| **type** | **name** | **description** |
|-|-|-|
| *address* | saleContract | not indexed |
| *uint256* | tokensForSale | not indexed |


---
# ERC20


## *function* approve

ERC20.approve(spender, value) `nonpayable` `095ea7b3`


Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* | spender | undefined |
| *uint256* | value | undefined |


## *function* totalSupply

ERC20.totalSupply() `view` `18160ddd`





## *function* transferFrom

ERC20.transferFrom(from, to, value) `nonpayable` `23b872dd`


Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* | from | undefined |
| *address* | to | undefined |
| *uint256* | value | undefined |


## *function* balanceOf

ERC20.balanceOf(who) `view` `70a08231`


Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* | who | undefined |


## *function* transfer

ERC20.transfer(to, value) `nonpayable` `a9059cbb`


Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* | to | undefined |
| *uint256* | value | undefined |


## *function* allowance

ERC20.allowance(owner, spender) `view` `dd62ed3e`


Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* | owner | undefined |
| *address* | spender | undefined |

## *event* Transfer

ERC20.Transfer(from, to, value) `ddf252ad`

Arguments

| **type** | **name** | **description** |
|-|-|-|
| *address* | from | indexed |
| *address* | to | indexed |
| *uint256* | value | not indexed |

## *event* Approval

ERC20.Approval(owner, spender, value) `8c5be1e5`

Arguments

| **type** | **name** | **description** |
|-|-|-|
| *address* | owner | indexed |
| *address* | spender | indexed |
| *uint256* | value | not indexed |

## *event* Burn

ERC20.Burn(from, value) `cc16f5db`

Arguments

| **type** | **name** | **description** |
|-|-|-|
| *address* | from | indexed |
| *uint256* | value | not indexed |

## *event* SaleContractActivation

ERC20.SaleContractActivation(saleContract, tokensForSale) `9ca62f58`

Arguments

| **type** | **name** | **description** |
|-|-|-|
| *address* | saleContract | not indexed |
| *uint256* | tokensForSale | not indexed |


---
# Owned


## *function* acceptOwnership

Owned.acceptOwnership() `nonpayable` `79ba5097`





## *function* owner

Owned.owner() `view` `8da5cb5b`





## *function* newOwner

Owned.newOwner() `view` `d4ee1d90`





## *function* transferOwnership

Owned.transferOwnership(_newOwner) `nonpayable` `f2fde38b`


Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* | _newOwner | undefined |


## *event* OwnershipTransferred

Owned.OwnershipTransferred(from, _to) `8be0079c`

Arguments

| **type** | **name** | **description** |
|-|-|-|
| *address* | from | indexed |
| *address* | _to | indexed |


---
# Pausable


## *function* unpause

Pausable.unpause() `nonpayable` `3f4ba83a`





## *function* paused

Pausable.paused() `view` `5c975abb`





## *function* acceptOwnership

Pausable.acceptOwnership() `nonpayable` `79ba5097`





## *function* pause

Pausable.pause() `nonpayable` `8456cb59`





## *function* owner

Pausable.owner() `view` `8da5cb5b`





## *function* newOwner

Pausable.newOwner() `view` `d4ee1d90`





## *function* transferOwnership

Pausable.transferOwnership(_newOwner) `nonpayable` `f2fde38b`


Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* | _newOwner | undefined |

## *event* Pause

Pausable.Pause() `6985a022`



## *event* Unpause

Pausable.Unpause() `7805862f`



## *event* OwnershipTransferred

Pausable.OwnershipTransferred(from, _to) `8be0079c`

Arguments

| **type** | **name** | **description** |
|-|-|-|
| *address* | from | indexed |
| *address* | _to | indexed |


---
# SafeMath


---
# StandardToken


## *function* approve

StandardToken.approve(_spender, _value) `nonpayable` `095ea7b3`

> Approve the passed address to spend the specified amount of tokens on behalf of msg.sender.   * Beware that changing an allowance with this method brings the risk that someone may use both the old and the new allowance by unfortunate transaction ordering. One possible solution to mitigate this race condition is to first reduce the spender's allowance to 0 and set the desired value afterwards: https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729

Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* | _spender | The address which will spend the funds. |
| *uint256* | _value | The amount of tokens to be spent. |


## *function* totalSupply

StandardToken.totalSupply() `view` `18160ddd`





## *function* transferFrom

StandardToken.transferFrom(_from, _to, _value) `nonpayable` `23b872dd`

> Allows allowed third party to transfer tokens from one address to another. Returns success

Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* | _from | Address from where tokens are withdrawn |
| *address* | _to | Address to where tokens are sent |
| *uint256* | _value | Number of tokens to transfer |

Outputs

| **type** | **name** | **description** |
|-|-|-|
| *bool* |  | undefined |

## *function* burn

StandardToken.burn(_value) `nonpayable` `42966c68`

> Burns a specific amount of tokens.

Inputs

| **type** | **name** | **description** |
|-|-|-|
| *uint256* | _value | The amount of token to be burned. |


## *function* balanceOf

StandardToken.balanceOf(_owner) `view` `70a08231`

> Returns number of tokens owned by given address

Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* | _owner | Address of token owner |

Outputs

| **type** | **name** | **description** |
|-|-|-|
| *uint256* |  | undefined |

## *function* changeApproval

StandardToken.changeApproval(_spender, _oldValue, _newValue) `nonpayable` `9281cd65`


Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* | _spender | undefined |
| *uint256* | _oldValue | undefined |
| *uint256* | _newValue | undefined |


## *function* transfer

StandardToken.transfer(_to, _value) `nonpayable` `a9059cbb`

> Transfers sender's tokens to a given address. Returns success

Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* | _to | Address of token receiver |
| *uint256* | _value | Number of tokens to transfer |

Outputs

| **type** | **name** | **description** |
|-|-|-|
| *bool* | success | undefined |

## *function* allowance

StandardToken.allowance(_owner, _spender) `view` `dd62ed3e`

> Function to check the amount of tokens that an owner allowed to a spender.

Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* | _owner | address The address which owns the funds. |
| *address* | _spender | address The address which will spend the funds. |

Outputs

| **type** | **name** | **description** |
|-|-|-|
| *uint256* |  | undefined |
## *event* Transfer

StandardToken.Transfer(from, to, value) `ddf252ad`

Arguments

| **type** | **name** | **description** |
|-|-|-|
| *address* | from | indexed |
| *address* | to | indexed |
| *uint256* | value | not indexed |

## *event* Approval

StandardToken.Approval(owner, spender, value) `8c5be1e5`

Arguments

| **type** | **name** | **description** |
|-|-|-|
| *address* | owner | indexed |
| *address* | spender | indexed |
| *uint256* | value | not indexed |

## *event* Burn

StandardToken.Burn(from, value) `cc16f5db`

Arguments

| **type** | **name** | **description** |
|-|-|-|
| *address* | from | indexed |
| *uint256* | value | not indexed |

## *event* SaleContractActivation

StandardToken.SaleContractActivation(saleContract, tokensForSale) `9ca62f58`

Arguments

| **type** | **name** | **description** |
|-|-|-|
| *address* | saleContract | not indexed |
| *uint256* | tokensForSale | not indexed |


---
## Token Vesting Contract:-
* [Owned](#owned)
  * [acceptOwnership](#function-acceptownership)
  * [owner](#function-owner)
  * [newOwner](#function-newowner)
  * [transferOwnership](#function-transferownership)
  * [OwnershipTransferred](#event-ownershiptransferred)
* [SafeMath](#safemath)
* [TokenVesting](#tokenvesting)
  * [beneficiaries](#function-beneficiaries)
  * [teamTokensSent](#function-teamtokenssent)
  * [teamVestableTokens](#function-teamvestabletokens)
  * [marketingVestableTokens](#function-marketingvestabletokens)
  * [releasableAmount](#function-releasableamount)
  * [release](#function-release)
  * [vestedAmount](#function-vestedamount)
  * [advisorsTokensSent](#function-advisorstokenssent)
  * [revoke](#function-revoke)
  * [acceptOwnership](#function-acceptownership)
  * [owner](#function-owner)
  * [released](#function-released)
  * [advisorsVestableTokens](#function-advisorsvestabletokens)
  * [marketingTokensSent](#function-marketingtokenssent)
  * [newOwner](#function-newowner)
  * [_token](#function-_token)
  * [transferOwnership](#function-transferownership)
  * [vestTokens](#function-vesttokens)
  * [revoked](#function-revoked)
  * [Released](#event-released)
  * [Revoked](#event-revoked)
  * [OwnershipTransferred](#event-ownershiptransferred)

# Owned


## *function* acceptOwnership

Owned.acceptOwnership() `nonpayable` `79ba5097`





## *function* owner

Owned.owner() `view` `8da5cb5b`





## *function* newOwner

Owned.newOwner() `view` `d4ee1d90`





## *function* transferOwnership

Owned.transferOwnership(_newOwner) `nonpayable` `f2fde38b`


Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* | _newOwner | undefined |


## *event* OwnershipTransferred

Owned.OwnershipTransferred(from, _to) `8be0079c`

Arguments

| **type** | **name** | **description** |
|-|-|-|
| *address* | from | indexed |
| *address* | _to | indexed |


---
# SafeMath


---
# TokenVesting


## *function* beneficiaries

TokenVesting.beneficiaries() `view` `01567739`


Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* |  | undefined |


## *function* teamTokensSent

TokenVesting.teamTokensSent() `view` `0799f2d3`





## *function* teamVestableTokens

TokenVesting.teamVestableTokens() `view` `07c24923`





## *function* marketingVestableTokens

TokenVesting.marketingVestableTokens() `view` `0fb84ff2`





## *function* releasableAmount

TokenVesting.releasableAmount(_beneficiary) `view` `1726cbc8`

> Calculates the amount that has already vested but hasn't been released yet.

Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* | _beneficiary | address of benefeciary for which tokens are getting vested |


## *function* release

TokenVesting.release(_beneficiary) `nonpayable` `19165587`

**Transfers vested tokens to beneficiary.**


Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* | _beneficiary | address of benefeciary for which tokens are getting vested |


## *function* vestedAmount

TokenVesting.vestedAmount(_beneficiary) `view` `384711cc`

> Calculates the amount that has already vested.

Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* | _beneficiary | address of benefeciary for which tokens are getting vested |


## *function* advisorsTokensSent

TokenVesting.advisorsTokensSent() `view` `53c0dc0d`





## *function* revoke

TokenVesting.revoke(_beneficiary) `nonpayable` `74a8f103`

**Allows the owner to revoke the vesting. Tokens already vested will be sent to benefeciary, the rest are returned back to contract for future vesting.**


Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* | _beneficiary | address of benefeciary for which tokens are getting vested |


## *function* acceptOwnership

TokenVesting.acceptOwnership() `nonpayable` `79ba5097`





## *function* owner

TokenVesting.owner() `view` `8da5cb5b`





## *function* released

TokenVesting.released() `view` `9852595c`


Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* |  | undefined |


## *function* advisorsVestableTokens

TokenVesting.advisorsVestableTokens() `view` `bba6a011`





## *function* marketingTokensSent

TokenVesting.marketingTokensSent() `view` `ceaed2fc`





## *function* newOwner

TokenVesting.newOwner() `view` `d4ee1d90`





## *function* _token

TokenVesting._token() `view` `ecd0c0c3`





## *function* transferOwnership

TokenVesting.transferOwnership(_newOwner) `nonpayable` `f2fde38b`


Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* | _newOwner | undefined |


## *function* vestTokens

TokenVesting.vestTokens(_beneficiary, _cliff, _duration, _tokens, _revocable, _member) `nonpayable` `f80284a0`

> Creates a vesting contract that vests its balance of any ERC20 token to the _beneficiary, gradually in a linear fashion until _start + _duration. By then all of the balance will have vested.

Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* | _beneficiary | address of the beneficiary to whom vested tokens are transferred |
| *uint256* | _cliff | the time duration in addition to current time after which  which vesting starts |
| *uint256* | _duration | duration in seconds of the period in which the tokens will vest |
| *uint256* | _tokens | undefined |
| *bool* | _revocable | whether the vesting is revocable or not |
| *uint256* | _member | ,uint value to identify vesting beneficiary ( 1 for core team,2 for advisor and 3 for marketing ) |


## *function* revoked

TokenVesting.revoked() `view` `fa01dc06`


Inputs

| **type** | **name** | **description** |
|-|-|-|
| *address* |  | undefined |


## *event* Released

TokenVesting.Released(amount) `fb81f9b3`

Arguments

| **type** | **name** | **description** |
|-|-|-|
| *uint256* | amount | not indexed |

## *event* Revoked

TokenVesting.Revoked() `b6fa8b8b`

Arguments

| **type** | **name** | **description** |
|-|-|-|
| *address* |  | not indexed |

## *event* OwnershipTransferred

TokenVesting.OwnershipTransferred(from, _to) `8be0079c`

Arguments

| **type** | **name** | **description** |
|-|-|-|
| *address* | from | indexed |
| *address* | _to | indexed |


---